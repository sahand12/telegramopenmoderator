FROM python:3.9

WORKDIR /app

RUN useradd -m ben1
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
COPY . . 

CMD ["/bin/bash"]
