# Telegram Open Moderator Bot

this is a telegram bot that deletes pornography in a group chat. It works with videos, GIFs and picture. 
It uses [Googles SafeSearch API](https://cloud.google.com/vision/docs/detecting-safe-search) to detect if the visual content posted in the group is inappropriate and if they are, the bot deletes the message and bans the user immediately. 

## Run
To run the bot. You need to set up the Google's [local development environment](https://cloud.google.com/docs/authentication/provide-credentials-adc#local-dev). Also create a text file and call it **.bottoken** in the project root directory and paste your telegram bot token in it.   

The project has not been maintained since couples of months ago. It probably still works. 
