import logging
from telegram import Update
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler, filters, MessageHandler
import requests
from google.cloud import vision
import cv2 
import random
import io
import numpy as np 
import os
from io import BytesIO
from PIL import Image


# Initialize the Telegram bot token
TOKEN = ''
GOOGLE_API_KEY = ''
with open('.bottoken','r') as f:
    TOKEN=f.readline().strip()
    
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = f"{os.getcwd()}/.googleapi"
i = 0  

allowedchatrooms = []
with open(".allowedchatrooms") as f:
            for line in f:
                allowedchatrooms.append(line.strip())

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
    #level=logging.DEBUG
)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

fh = logging.FileHandler('info.log')
fh.setLevel(logging.DEBUG)

eh = logging.FileHandler('error.log')
eh.setLevel(logging.ERROR)


def get_random_frames(video_url, num_frames_in1000):
    cap = cv2.VideoCapture(video_url)
    total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) 
    num_frames = int(total_frames/1000*num_frames_in1000)
    if num_frames == 0: 
        num_frames = 1
    random_frame_indices = random.sample(range(0, total_frames), num_frames)
    frames = []
    for frame_index in random_frame_indices:
        cap.set(cv2.CAP_PROP_POS_FRAMES, frame_index)
        ret, frame = cap.read()
        if ret:
            frames.append(frame)
    cap.release()
    logger.debug("type of frames is: ", type(frames))
    return frames

async def handler_callback_text(update, context):
    message = update.message
    user_id = message.from_user.id
    text = message.text
    logger.debug("the message said: ", text)
    logger.debug("this is the contect.args: ", context.args)
    # Check if the message contains pornography or drug-related keywords
    if 'pornography' in text or 'drugs' in text:
        # Remove the user from the chat
        await context.bot.kick_chat_member(chat_id=message.chat_id, user_id=user_id)

async def detect_safe_search(img):
    client = vision.ImageAnnotatorClient()
    # if type(img) is str:
    #     with open(img, 'rb') as image_file:
    #         content = image_file.read()
    if type(img) is np.ndarray: 
        success, encoded_image = cv2.imencode('.png', img)
        content = encoded_image.tobytes()
    else:
        logging.error("format not correct")
    image = vision.Image(content=content)
    response = client.safe_search_detection(image=image)
    safe = response.safe_search_annotation
    logger.debug("there is an image")
    # Names of likelihood from google.cloud.vision.enums
    #likelihood_name = ('0-UNKNOWN', '1-VERY_UNLIKELY', '2-UNLIKELY', '3-POSSIBLE',
    #                   '4-LIKELY', '5-VERY_LIKELY')
    if response.error.message:
        raise Exception(
            '{}\nFor more info on error messages, check: '
            'https://cloud.google.com/apis/design/errors'.format(
                response.error.message))
    return safe

class OpenModerator:
    def __init__(self,update,context):
        self.message = update.message 
        self.user_id = self.message.from_user.id
        self.context = context 
        logger.debug(f"the message is: {self.message}")
        global i 
    def chatallowed(self):
        if str(self.message.chat_id) in allowedchatrooms: 
            return True
        else: 
            return False

    async def handleFrame(self, frame):
        safe = await detect_safe_search(frame)
        logger.debug(f"image has safe value: {safe.adult}")
        if int(safe.adult) >= 2: 
            await self.message.delete()
        if int(safe.adult) > 3: 
            logger.info("user:{} is kicked!".format(self.user_id)) 
            await self.context.bot.ban_chat_member(chat_id=self.message.chat_id,user_id=self.user_id, revoke_messages=True)

    async def handleFrames(self, random_frames):
        for frame in random_frames: 
            await self.handleFrame(frame)

    async def handlemessage(self):
        if not(self.chatallowed()): 
            logger.debug("chat not allowed")
            return
        if self.message.photo:
            file_info = await self.context.bot.get_file(self.message.photo[-1].file_id)
            image_bytes = requests.get(file_info.file_path).content
            byte_stream = BytesIO(image_bytes)
            image = Image.open(byte_stream)
            np_image = np.array(image)
            # print(type(image)) 
            # global i 
            # i += 1 
            # uri = "image{}.jpg".format(i)
            # image.save(uri)
            await self.handleFrame(np_image)

        if  self.message.animation: 
            logger.debug("message at least an animation!")
            logger.debug("message.animation[-1].file_id", self.message.animation["file_id"])
            file_info = await self.context.bot.get_file(self.message.animation["file_id"])
            logger.debug("file_info is: ", file_info)
            random_frames = get_random_frames(file_info.file_path, 5)
            await self.handleFrames(random_frames)
        if self.message.video: 
            logger.debug("message has at least a video")
            logger.debug("message.animation[-1].file_id", self.message.video["file_id"])
            file_info = await self.context.bot.get_file(self.message.video["file_id"])
            logger.debug("file_info is: ", file_info)
            #logger.debug("type of video file is ", type(video_file))
            random_frames = get_random_frames(file_info.file_path, 5)
            await self.handleFrames(random_frames)
        if self.message.document: 
            file_info = await self.context.bot.get_file(self.message.document["file_id"])
            if self.message.document["mime_type"] == 'image/gif': 
                random_frames = get_random_frames(file_info.file_path, 5)
                await self.handleFrames(random_frames)
        if self.message.new_chat_members: 
            logger.debug(f"new chat memeber added: {self.message.new_chat_members[-1]}")
            photo_info_list = await self.context.bot.get_user_profile_photos(user_id=self.message.new_chat_members[-1].id)
            for i in range(photo_info_list.total_count):  
                photo_info= photo_info_list.photos[i] 
                file_info = await self.context.bot.get_file(photo_info[-1].file_id)
                image_bytes = requests.get(file_info.file_path).content
                byte_stream = BytesIO(image_bytes)
                image = Image.open(byte_stream) # .jpg
                np_image = np.array(image)
                safe = await detect_safe_search(np_image) 
                if int(safe.adult) >4:
                    logging.info(f"user {self.context.bot.new_chat_members[-1].id} is removed")
                    await self.context.bot.ban_chat_member(chat_id=self.message.chat_id,user_id=self.user_id, revoke_messages=True)


async def handler_callback_all(update, context):
    moderator = OpenModerator(update, context)
    await moderator.handlemessage()
    return 


#async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
#    await context.bot.send_message(cha"t_id=update.effective_chat.id, text="I'm a bot, please talk to me!")
import asyncio
if __name__ == '__main__': 
    #logger.debug("server started")
    application = ApplicationBuilder().token(TOKEN).build()

    #application.add_handler(MessageHandler(filters.TEXT, handler_callback_text))
    application.add_handler(MessageHandler(filters.ALL, handler_callback_all))
    #updater.start_polling()
    application.run_polling()

